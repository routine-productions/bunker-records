(function ($) {
    $(document).ready(function () {

        $('.JS-Scroll-Button').click(function () {

            var Scroll_Point_Duration = $(this).attr('data-scroll-point-duration') ? $(this).attr('data-scroll-point-duration') : 800;

            var Id = $(this).attr('href').replace('/', '');
            var Scroll_Position = $(Id).offset().top;

            $('body,html').animate({
                scrollTop: Scroll_Position
            }, Scroll_Point_Duration);

            return false;
        });
    });
})(jQuery);





