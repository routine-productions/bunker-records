(function ($) {
    var Modal_Button = $('.Order'),
        Modal_Hidden = $('.Feedback-Wrap'),
        Modal_Hidden_Zone = 'Feedback',
        Modal_Active = 'Active';

    $(Modal_Button).click(function () {
        var $Title = $(this).parents('.Content').find('h2').text();

        Modal_Hidden.toggleClass(Modal_Active).find('h2').text($Title);
        return false;
    });

    $('body').click(function (Event) {
        if ($(Modal_Hidden).hasClass(Modal_Active)) {
            if ($(Event.target).hasClass(Modal_Hidden_Zone)) {
                $(Modal_Hidden).removeClass(Modal_Active);
            }
        }
    });
})(jQuery);


    


