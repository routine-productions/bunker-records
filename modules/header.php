<header class="Header">

    <div class="Header-Top">
            <svg class="Rectangle">
                <use xlink:href="#rectangle"></use>
            </svg>
            <img src="/img/bunker-logo.png" alt="">

    </div>
    <div class="Header-Content">
        <div class="Title">
            <h1>Студия звукозаписи в Мариуполе!</h1>
            <a href="#about" class="JS-Scroll-Button">Подробнее</a>
        </div>

    </div>

</header>