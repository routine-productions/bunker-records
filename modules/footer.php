
            <span class="Copyright">
                <span>© Bunker-A, студия&nbsp;звукозаписи, 2016</span>
            </span>
            <div class="Routine-Productions">
                <a href="http://routine.productions/">
                    <svg>
                        <defs>
                            <radialGradient id="Sign_Monochrome_1_" cx="252.05" cy="167.5502" r="138.8421" gradientTransform="matrix(1 0 0 -1 0 273.6)" gradientUnits="userSpaceOnUse">
                                <stop  offset="0" style="stop-color:#E74C3C"/>
                                <stop  offset="1" style="stop-color:#E75A37"/>
                            </radialGradient>
                        </defs>
                        <use xlink:href="#routine-logo"></use>
                    </svg>
                    <span>production studio</span>

                </a>
            </div>

