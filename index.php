<!DOCTYPE html>
<html>

<head lang="ru">
    <meta charset="UTF-8">
    <title>Bunker-Records</title>

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700&subset=latin,cyrillic' rel='stylesheet'
          type='text/css'>

    <link rel="stylesheet" href="/css/index.min.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
    require_once './modules/favicon.php';
    ?>
</head>

<body>
<?php
require_once './modules/svg-gradient.php';
require __DIR__ . "/img/sprite.svg";
require_once __DIR__ . '/libs/Mobile_Detect.php';
$Detect = new Mobile_Detect;
$Dir_img = __DIR__ . '/img/';
?>

<div class="Header-Wrap">

    <?php
    require './modules/header.php';
    ?>

</div>


<main>

    <?php
    require './modules/main.php';
    ?>


</main>
<footer class="Site-Footer">
    <?php
    require './modules/footer.php';
    ?>
</footer>


<?php
require './modules/feedback_modal.php';
?>

<script src="/index.min.js"></script>
</body>
</html>

